<?php

require("../../CONNECTION/SECURITY/conex.php");

$id_userd = base64_decode($_POST['id_user']);
$id_pap = $_POST['id_pap'];
$id_paciente = 'hola';
$telefono = $_POST['telefono'];
$direccion = $_POST['direccion'];
$ciudad = $_POST['ciudad'];
$departamento = $_POST['departamento1'];
$nombreCuid = $_POST['nombreCuid'];
$parentesco = $_POST['parentesco'];
$telAcudiente = $_POST['telAcudiente'];
$fechProxGestion = $_POST['fechProxGestion'];
$descripcion = $_POST['descripcion'];
$activar_tol =  $_POST['activar_tol'];


//Inset general de la gestion en caso que no aya una gestion anclada
$insert_gestion = mysqli_query($conex, "INSERT INTO `gestion`( `id_user`, `id_paciente`, `descripcion`, `fech_proxgestion`, `fecha_reg`, `estado`) VALUES ('$id_userd','$id_pap', '$descripcion', '$fechProxGestion', NOW(), '1')");

//Si ya hay gestion actualiza
$update_gestion = mysqli_query($conex, "UPDATE `gestion` SET `id_user`='$id_userd', `descripcion`='$descripcion',`fech_proxgestion`='$fechProxGestion',`fecha_reg`= NOW(),`estado`='1' WHERE `id_paciente` = '$id_pap'");

// Inserta para tener el historico de las gestiones
$insert_gestion_historico = mysqli_query($conex, "INSERT INTO `gestion_historical`( `id_user`, `id_paciente`, `descripcion`, `fech_proxgestion`, `fecha_reg`) VALUES ('$id_userd','$id_pap', '$descripcion', '$fechProxGestion', NOW())");

//Notifica el cambio de edicion en algunos campos
if ($activar_tol == 'on') {
  if (isset($_POST['activar_nom'])) {
    $nombre = $_POST['nombre'];
  } else {
    $nombre = 'NA';
  }
  if (isset($_POST['activar_apell'])) {
    $apellido = $_POST['apellido'];
  } else {
    $apellido = 'NA';
  }
  if (isset($_POST['activar_cedu'])) {
    $cedula = $_POST['cedula'];
  } else {
    $cedula = '0';
  }
  if (isset($_POST['activar_ips'])) {
    $ips = $_POST['ips'];
  } else {
    $ips = 'NA';
  }
  if (isset($_POST['activar_cie10'])) {
    $cie10 = $_POST['cie10'];
  } else {
    $cie10 = 'NA';
  }
  if (isset($_POST['activar_diag'])) {
    $diagnostico = $_POST['diagnostico'];
  } else {
    $diagnostico = 'NA';
  }

  $activar_est = $_POST['activar_est'];
  if ($activar_est == 'on') {
    $estado = $_POST['estadoes'];
    $causal = $_POST['causal1'];
  } else {
    $estado = 'NA';
    $causal = 'NA';
  }

  if (($nombre != '') || ($apellido != '') || ($cedula != '') || ($ips != '') || ($cie10 != '') || ($diagnostico != '') || ($activar_est != '')) {

    $insert_datos = mysqli_query($conex, "INSERT INTO `estado_temporal`( `id_user`, `id_paciente`, `estado`, `causal`, `nombre`, `apellido`, `documento`, `ips`, `cie10`, `diagnostico`, `fecha_reg`) VALUES ('$id_userd','$id_pap', '$estado', '$causal', '$nombre' , '$apellido', '$cedula', '$ips', '$cie10', '$diagnostico', NOW())");

    if ($insert_datos == TRUE) {
      require('../INTERACTIVE/GLOBAL_PHP/mail_solicitud_cambio.php');
      print '
        
      <script>
      
          alert("Se envio cambio de Solicitud PAP '. $id_pap .'");
      window.location.assign("../../MAIN_FOLDER/MODULES/SHARED/tabla_seguimiento.php");
    
     
      
      </script>
      
      <body style="font-family: Helvetica, Arial, sans-serif"> </body>' ;
    } else {
      print '
        
      <script>
      
          alert("Ya hay una solicitud sobre este PAP '. $id_pap .'");
      window.location.assign("../../MAIN_FOLDER/MODULES/SHARED/tabla_seguimiento.php");
    
     
      
      </script>
      
      <body style="font-family: Helvetica, Arial, sans-serif"> </body>' ;
      
    }
  }
}
