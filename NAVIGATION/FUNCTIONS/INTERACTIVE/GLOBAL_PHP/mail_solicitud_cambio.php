<?php

date_default_timezone_set('America/Bogota');
$today = date("Y-m-d");
$time = date ("G:i:h");
$mail_to = "mailto:notificacion_alivius@app-peoplemarketing.com";
$subject = "app Alivius"; 
$subject_ase = "Solicitud de cambio Alivius PAP ".$id_pap; 
$copia_a = "desarrollo@app-peoplemarketing.com";

$mensaje_paciente = "<!DOCTYPE html>
<html lang='es'>
<head>
    <meta charset='UTF-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <title>Correo</title>
</head>
<body style='font-family: Helvetica, Arial, sans-serif'>
    
    <h3><b>Solicitud Cambio PAP ".$id_pap."</b></h3>
    <main>
    <span><b>Nombre /Apellido:</b> ".$nombre." /".$apellido."</span><br>
    <span><b>Documento:</b> ".$cedula."</span><br>
    <span><b>IPS:</b> ".$ips."</span><br>
    <span><b>CIE10:</b> ".$cie10."</span><br>
    <span><b>Diagnostico:</b> ".$diagnostico."</span><br>
        <span><b>Solicitud de cambio Estado /Causal:</b> ".$estado." /".$causal."</span><br>   
        <span><b>Observaci&oacute;n:</b> ".$descripcion."</span><br><br> <br>

    <span style='font-size:11px;'><b>Nota:</b>Es un mensaje directo del aplicativo People Marketing S.A.S (Alivius), no responder</span><br>
</main>

    
</body>
</html>";

$cabeceras  = 'MIME-Version: 1.0' . "\r\n";
$cabeceras .= 'Content-type: text/html; charset=utf-8' . "\r\n";
//cabeceras adicionales<br />
$cabeceras .= 'From: App Alivius<notificacion_alivius@app-peoplemarketing.com>'."\r\n";
//con esto guardamos la URL en la variable enlace....
//ahora bien esta URL es la que toma por defecto en la pagina que corre este mismo script
//espero se entienda puedes hacer una prueba poniendo
$select_correo_asesor = mysqli_query($conex, "SELECT U.`correo` AS correo FROM `userlogin` AS UL LEFT JOIN `user` AS U ON UL.`id_user` = U.`id_user` WHERE UL.`id_log` = '$id_userd'");
    while ($dato_corrA = mysqli_fetch_array($select_correo_asesor)) {
        $correo_user = $dato_corrA['correo'];
        mail($correo_user,$subject_ase,$mensaje_paciente,$cabeceras);
        
    } 

    $select_correo_admin = mysqli_query($conex, "SELECT U.`correo` AS correo FROM `userlogin` AS UL LEFT JOIN `user` AS U ON UL.`id_user` = U.`id_user` WHERE UL.`id_loginrol` = '1'");
    while ($dato_corrAD = mysqli_fetch_array($select_correo_admin)) {
        $correo_admin = $dato_corrAD['correo'];
        mail($correo_admin,$subject_ase,$mensaje_paciente,$cabeceras);
        
    }
   
mail($copia_a,$subject_ase,$mensaje_paciente,$cabeceras);
