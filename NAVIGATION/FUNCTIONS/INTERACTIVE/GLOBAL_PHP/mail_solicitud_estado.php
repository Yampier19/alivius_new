<?php

date_default_timezone_set('America/Bogota');
$today = date("Y-m-d");
$time = date ("G:i:h");
$mail_to = "mailto:notificacion_alivius@app-peoplemarketing.com";
$subject = "app Alivius"; 
$subject_ase = "Solicitud de cambio Estado Alivius PAP ".$id_paciente; 
$copia = "soporte@peoplecontact.cc";
$copia_a = "desarrollo@app-peoplemarketing.com";
$mensaje_paciente = "<!DOCTYPE html>
<html lang='es'>
<head>
    <meta charset='UTF-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <title>Correo</title>
</head>
<body style='font-family: Helvetica, Arial, sans-serif'>
    
    <h3><b>Solicitud Cambio de Estado PAP ".$id_paciente."</b></h3>
    <main>
    <span><b>Nombre:</b> ".$nombre." ".$apellido."</span><br>
    <span><b>Departamento /Ciudad:</b> ".$nomCiu." /".$ciudad."</span><br>
    <span><b>Actual Estado /Causal:</b> Juan</span><br>
    <span><b>Solicitud de cambio Estado /Causal:</b> ".$estado." /".$causal."</span><br> <br>   
    
    <span style='font-size:11px;'><b>Nota:</b>Es un mensaje directo del aplicativo Alivius, no responder</span><br>
</main>

    
</body>
</html>";

$cabeceras  = 'MIME-Version: 1.0' . "\r\n";
$cabeceras .= 'Content-type: text/html; charset=utf-8' . "\r\n";
//cabeceras adicionales<br />
$cabeceras .= 'From: App Alivius<notificacion_alivius@app-peoplemarketing.com>'."\r\n";
//con esto guardamos la URL en la variable enlace....
//ahora bien esta URL es la que toma por defecto en la pagina que corre este mismo script
//espero se entienda puedes hacer una prueba poniendo
mail($copia,$subject_ase,$mensaje_paciente,$cabeceras);
mail($copia_a,$subject_ase,$mensaje_paciente,$cabeceras);
