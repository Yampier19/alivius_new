<?php 
 public function payQuickerReport()
 {
     $market = array(1 => 'US', 2 => 'CA', 3 => 'AU', 4 => 'NZ', 5 => 'GB');
     //        error_reporting(E_ALL);
     $data = $this->RoyaltiesEarned->getRoyaltiesReport(3, ['method' => 'all']);
     //        echo '<pre>';
     //        print_r($data);
     //        echo '</pre>';
     //        die;
     //        for ($i = 0; $i <= $queryCount; $i = $i + $this->payQuickerReportLimit) {
     //
     //        }
     $objPHPExcel = new PHPExcel();
     // Set properties
     $objPHPExcel->getProperties()->setCreator("");
     $objPHPExcel->getProperties()->setLastModifiedBy("");
     $objPHPExcel->getProperties()->setTitle("");
     $objPHPExcel->getProperties()->setSubject("");
     $objPHPExcel->getProperties()->setDescription("");
     $objPHPExcel->setActiveSheetIndex(0);
     $objPHPExcel->getActiveSheet()->setTitle('Instructions');
     // Add some data
     $objPHPExcel->createSheet(1);
     $objPHPExcel->setActiveSheetIndex(1);
     $objPHPExcel->getActiveSheet()->setTitle('Instant Payments');
     $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'INSTANT PAYMENTS');
     $objPHPExcel->getActiveSheet()->SetCellValue('A2', "RECIPIENT'S EMAIL ADDRESS\n * Required!");
     $objPHPExcel->getActiveSheet()->SetCellValue('B2', "PAYMENT AMOUNT\n * Required");
     $objPHPExcel->getActiveSheet()->SetCellValue('C2', "COUNTRY CODE\n * Required");
     $objPHPExcel->getActiveSheet()->SetCellValue('D2', "STATE CODE\n * Required");
     $objPHPExcel->getActiveSheet()->SetCellValue('E2', "COMMENT\n Optional");
     $objPHPExcel->getActiveSheet()->SetCellValue('F2', "SECURITY ID\n Optional");
     $objPHPExcel->getActiveSheet()->SetCellValue('G2', "SECURITY ID HINT\n Optional");
     $objPHPExcel->getActiveSheet()->SetCellValue('H2', "ACCOUNTING ID\n Optional");
     $objPHPExcel->getActiveSheet()->SetCellValue('I2', "EXPIRATION DATE\n Optional");
     $objPHPExcel->getActiveSheet()->SetCellValue('J2', "UDF1\n Optional");
     $objPHPExcel->getActiveSheet()->SetCellValue('K2', "UDF2\n Optional");
     $objPHPExcel->getActiveSheet()->SetCellValue('L2', "UDF3\n Optional");
     $objPHPExcel->getActiveSheet()->SetCellValue('M2', "AUTO ISSUE DEBIT CARDS\n Optional");
     $objPHPExcel->getActiveSheet()->SetCellValue('N2', "EXCLUDE FROM 1099 RECONCILIATION\n Optional");
     // Add data
     for ($i = 0; $i < count($data); $i++) {
         $cell = $i + 3;
         $objPHPExcel->getActiveSheet()->setCellValue('A' . $cell, $data[$i]['Email']['email'])->setCellValue('B' . $cell, $data[$i]['RoyaltiesEarned']['amount'])->setCellValue('C' . $cell, $market[$data[$i]['RoyaltiesEarned']['market_id']])->setCellValue('D' . $cell, $data[$i]['State']['abbrev'])->setCellValue('D' . $cell, $data[$i]['State']['abbrev'])->setCellValue('E' . $cell, $data[$i]['User']['first_name'])->setCellValue('M' . $cell, 'YES');
     }
     $objPHPExcel->createSheet(2);
     $objPHPExcel->setActiveSheetIndex(2);
     $objPHPExcel->getActiveSheet()->setTitle('Version');
     $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Version');
     $objPHPExcel->getActiveSheet()->SetCellValue('A2', '816eda7a-db7a-4c83-adeb-206bdfad2bb0');
     // Save Excel 2007 file
     $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
     // We'll be outputting an excel file
     //        header('Content-type: application/vnd.ms-excel');
     // It will be called file.xls
     //        header('Content-Disposition: attachment; filename="file.xls"');
     // Write file to the browser
     //        $objWriter->save('php://output');
     $objWriter->save(str_replace('.php', '.xlsx', __FILE__));
 }

 ?>