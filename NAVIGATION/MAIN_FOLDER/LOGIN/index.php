<!doctype html>
<html lang="es">
  <head>
    <title>Novo</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
<meta name="mobile-web-app-capable" content="yes">

    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <!-- Material Dashboard CSS -->
    <link href="../../DESIGN/assets/demo/material-dashboard.min.css" rel="stylesheet" >
<script src="../../DESIGN/JS/jquery-3.5.1.min.js" ></script>
<script type="text/javascript">
    function ident() {
        var dXNlcmxvZw = $('#dXNlcmxvZw').val();
		var cGFzc3dvcmRsb2c = $('#cGFzc3dvcmRsb2c').val();
        $.ajax(
                {
                    url:'../../FUNCTIONS/CRUD/login_user.php',

                        data:
                                {
                                    dXNlcmxvZw: dXNlcmxvZw,
									cGFzc3dvcmRsb2c: cGFzc3dvcmRsb2c
                                },
                        type: 'post',
                        success: function (data)
                        {
                            $('#alerta').html(data);
                        }
                    });
        }
</script>

<script type="text/javascript">
    $(document).ready(function () {
   $('#enviar').click(function ()
        {
            ident();
        });  
});
</script> 
<style>
   body {
   /* Valores de atributo clave */
overscroll-behavior: auto; 
overscroll-behavior: contain;
overscroll-behavior: none;

/* Dos valores (X y Y respectivamente) */
overscroll-behavior: auto contain;

/* Valores Globales */
overflow: inherit;
overflow: initial;
overflow: unset;
  overscroll-behavior: contain;
}</style>
  </head>
  <body class="off-canvas-sidebar">
 <div class="wrapper wrapper-full-page">
    <div class="page-header login-page header-filter" filter-color="black" style="background-image: url('../../DESIGN/IMG/fondo-min.jpg'); background-size: cover; background-position: top center; background-color: rgba(0,188,212,.4);">
      <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">
              <div class="card card-login">
                <div class="card-header card-header-rose text-center">
                  <h4 class="card-title"><img src="../../DESIGN/IMG/ALIVIUS3.png" width="250px"></h4>
                </div>
                <div class="card-body ">
                  <p class="card-description text-center"></p>
                  <span class="bmd-form-group">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="material-icons">perm_identity</i>
                        </span>
                      </div>
                      <input type="text" class="form-control" name="dXNlcmxvZw"  id="dXNlcmxvZw"  placeholder="Usuario..." required = "required">
                    </div>
                  </span>
                  <span class="bmd-form-group">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="material-icons">lock_outline</i>
                        </span>
                      </div>
                      <input type="password" class="form-control" name="cGFzc3dvcmRsb2c" id="cGFzc3dvcmRsb2c" placeholder="Password..." required = "required">
                    </div>
                  </span>
                </div>
				<br>
                <div class="card-footer justify-content-center">
                  <button name="enviar" id="enviar" class="btn btn-info btn-info btn-lg">Ingresar</button>
                </div>
              </div>
			<div id="alerta"></div>
          </div>
        </div>
      </div>
      <footer class="footer">
        <div class="container">
          <div class="copyright float-right">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script>
            <a href="https://peoplemarketing.com/inicio/" target="_blank">People Marketing</a>
            <!--<a href='https://www.freepik.es/fotos/ribbon'>Foto de Ribbon creado por freepik - www.freepik.es</a>-->
          </div>
        </div>
      </footer>
    </div>
  </div>
<script>
    function setFormValidation(id) {
      $(id).validate({
        highlight: function(element) {
          $(element).closest('.form-group').removeClass('has-success').addClass('has-danger');
          $(element).closest('.form-check').removeClass('has-success').addClass('has-danger');
        },
        success: function(element) {
          $(element).closest('.form-group').removeClass('has-danger').addClass('has-success');
          $(element).closest('.form-check').removeClass('has-danger').addClass('has-success');
        },
        errorPlacement: function(error, element) {
          $(element).closest('.form-group').append(error);
        },
      });
    }

    $(document).ready(function() {
      setFormValidation('#RegisterValidation');
      setFormValidation('#TypeValidation');
      setFormValidation('#LoginValidation');
      setFormValidation('#RangeValidation');
    });
  </script>
  <!--   Core JS Files   -->

  <script src="../../DESIGN/assets/js/core/popper.min.js" type="text/javascript"></script>
  <script src="../../DESIGN/assets/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>


  <!-- Plugin for the Perfect Scrollbar -->
  <script src="../../DESIGN/assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>

  <!-- Plugin for the momentJs  -->
  <script src="../../DESIGN/assets/js/plugins/moment.min.js"></script>

  <!--  Plugin for Sweet Alert -->
  <script src="../../DESIGN/assets/js/plugins/sweetalert2.js"></script>

  <!-- Forms Validations Plugin -->
  <script src="../../DESIGN/assets/js/plugins/jquery.validate.min.js"></script>

  <!--  Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
  <script src="../../DESIGN/assets/js/plugins/jquery.bootstrap-wizard.js"></script>

  <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
  <script src="../../DESIGN/assets/js/plugins/bootstrap-selectpicker.js" ></script>

  <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
  <script src="../../DESIGN/assets/js/plugins/bootstrap-datetimepicker.min.js"></script>

  <!--  DataTables.net Plugin, full documentation here: https://datatables.net/    -->
  <script src="../../DESIGN/assets/js/plugins/jquery.dataTables.min.js"></script>

  <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
  <script src="../../../DESIGN/assets/js/plugins/bootstrap-tagsinput.js"></script>

  <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
  <script src="../../DESIGN/assets/js/plugins/jasny-bootstrap.min.js"></script>

  <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
  <script src="../../DESIGN/assets/js/plugins/fullcalendar.min.js"></script>

  <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
  <script src="../../DESIGN/assets/js/plugins/jquery-jvectormap.js"></script>

  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="../../DESIGN/assets/js/plugins/nouislider.min.js" ></script>

  <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>

  <!-- Library for adding dinamically elements -->
  <script src="../../DESIGN/assets/js/plugins/arrive.min.js"></script>

  <!--  Google Maps Plugin    -->
  <script  src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

  <!-- Chartist JS -->
  <script src="../../DESIGN/assets/js/plugins/chartist.min.js"></script>

  <!--  Notifications Plugin    -->
  <script src="../../DESIGN/assets/js/plugins/bootstrap-notify.js"></script>

  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="../../../DESIGN/assets/demo/material-dashboard.js" type="text/javascript"></script>

  </body>
</html>