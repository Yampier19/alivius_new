<?php      
require('../../../CONNECTION/SECURITY/conex.php');

//session_start();
$string_intro = getenv("QUERY_STRING"); 
parse_str($string_intro);

//Exportar datos de php a Excel
header("Content-Type: application/vnd.ms-excel");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("content-disposition: attachment;filename=reports_digital_form.xls");

  
  
  $consulta_paciente=mysqli_query($conex,"SELECT * FROM `saxenda` AS A LEFT JOIN pdv_farmacia as B ON A.id_pdv =B.id_pdv LEFT JOIN user AS C ON A.id_user = C.id_user  WHERE 1");
  echo mysqli_error($conex);

?>
<table border="1px" bordercolor="#000000">
      <tr style="font-weight:bold; text-transform:uppercase; height:25; padding:3px">
          
		  <th class="botones" style="background-color:#23697b; color: #ffffff;" >COD</th>
		  <th class="botones" style="background-color:#23697b; color: #ffffff;" >NOMBRE PDV</th>
		  <th class="botones" style="background-color:#23697b; color: #ffffff;" >CIUDAD</th>
          <th class="botones" style="background-color:#23697b; color: #ffffff;" >DIRECCI&Oacute;N</th>
          <th class="botones" style="background-color:#23697b; color: #ffffff;" >TELEFONO</th>
          <th class="botones" style="background-color:#23697b; color: #ffffff;" >CADENA PDV</th>
          <th class="botones" style="background-color:#23697b; color: #ffffff;" >NOMBRE REGENTE</th>
          <th class="botones" style="background-color:#23697b; color: #ffffff;" >N&ordm; REG.</th>
          <th class="botones" style="background-color:#23697b; color: #ffffff;" >CONVENIO</th>
          <th class="botones" style="background-color:#23697b; color: #ffffff;" >CAT.</th>
		  <th class="botones" style="background-color:#23697b; color: #ffffff;" >TIPO DE GESTION</th>
		  <th class="botones" style="background-color:#23697b; color: #ffffff;" >CUENTA CON STOCK</th>
          <th class="botones" style="background-color:#23697b; color: #ffffff;" >PORQUE NO CUENTA CON STOCK</th>
		  <th class="botones" style="background-color:#23697b; color: #ffffff;" >MOTIVO DE AGOTADO</th>
		  <th class="botones" style="background-color:#23697b; color: #ffffff;" >FECHA APROXIMADAS DE LLEGADA</th>
		  <th class="botones" style="background-color:#23697b; color: #ffffff;" >OBSERVACI&Oacute;N ALERTA</th>
		  <th class="botones" style="background-color:#23697b; color: #ffffff;" >ROTACI&Oacute;N DE LA SEMANA</th>
		  <th class="botones" style="background-color:#23697b; color: #ffffff;" >CANTIDAD UNI. CAJA X 1</th>
		  <th class="botones" style="background-color:#23697b; color: #ffffff;" >PRECIO CAJA X 1</th>
		  <th class="botones" style="background-color:#23697b; color: #ffffff;" >CANTIDAD UNI. CAJA X 3</th>
		  <th class="botones" style="background-color:#23697b; color: #ffffff;" >PRECIO CAJA X 3</th>
		  <th class="botones" style="background-color:#23697b; color: #ffffff;" >CUENTA CON DESCUENTOS</th>
		  <th class="botones" style="background-color:#23697b; color: #ffffff;" >DESCUENTO</th>
		  <th class="botones" style="background-color:#23697b; color: #ffffff;" >PORCENTAJE</th>
		  <th class="botones" style="background-color:#23697b; color: #ffffff;" >OBSERVACI&Oacute;N</th>
		  <th class="botones" style="background-color:#23697b; color: #ffffff;" >FECHA DE REGISTRO</th>
		  <th class="botones" style="background-color:#23697b; color: #ffffff;" >USUARIO REGISTRO</th>
         
          
      </tr>
	  <?php
      while ($fila1 = mysqli_fetch_array($consulta_paciente))
      { 
      ?>
          <tr align="center" style="height:25px;">
          	  
              
			  <td><?php echo $fila1['cod_pdv']; ?></td>
			  <td><?php echo utf8_decode($fila1['nombre_pdv']); ?></td>
			  <td><?php echo utf8_decode($fila1['ciudad_pdv']); ?></td>
              <td><?php echo utf8_decode($fila1['direccion_pdv']);?></td>
			  <td><?php echo utf8_decode($fila1['telefono']);?></td>
			  <td><?php echo utf8_decode($fila1['cadena_pdv']);?></td>
			  <td><?php echo utf8_decode($fila1['nombre_regente']);?></td>
			  <td><?php echo $fila1['num_regetente'];?></td>
			  <td><?php echo utf8_decode($fila1['convenio']);?></td>
			  <td><?php echo $fila1['categoria']?></td>
			  <td><?php echo utf8_decode($fila1['tipo_gestion']);?></td>
			  <td><?php echo $fila1['cuenta_stock']?></td>
			  <td><?php echo $fila1['no_cuenta_stock'];?></td>
			  <td><?php echo $fila1['motivo_agotado'];?></td>
			  <td><?php echo $fila1['fecha_prox_llegada']?></td>
			  <td><?php echo $fila1['alerta_observacion']?></td>
			  <td><?php echo $fila1['rotacion_x_semana'];?></td>
			  <td><?php echo $fila1['cantidad_cajax1']?></td>
              <td><?php echo $fila1['precio_cajax1']?></td>
              <td><?php echo $fila1['cantidad_cajax3']?></td>
              <td><?php echo $fila1['precio_cajax3']?></td>
              <td><?php echo $fila1['cuenta_descuentos']?></td>
			  <td><?php echo $fila1['descuento']?></td>
              <td><?php echo $fila1['porcentaje']?></td>
			  <td><?php echo $fila1['observacion_nord']?></td>
			  <td><?php echo $fila1['fecha_registro']?></td>
              <td><?php echo $fila1['names']." ".$fila1['surnames']; ?></td>             
			  </tr>
              <?php
			 
      }
      ?>
 </table>