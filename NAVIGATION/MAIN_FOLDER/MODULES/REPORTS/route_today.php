<?php      
require('../../../CONNECTION/SECURITY/conex.php');

//session_start();
$string_intro = getenv("QUERY_STRING"); 
parse_str($string_intro);
date_default_timezone_set('America/Bogota');
//Exportar datos de php a Excel
header("Content-Type: application/vnd.ms-excel");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("content-disposition: attachment;filename=reports_digital_form.xls");

$date_hoy = date('Y-m-d');
  
  
  $consulta_cov19=mysqli_query($conex,"SELECT * FROM pdv_farmacia AS D LEFT JOIN `pdv_efectivo` AS B ON B.id_pdv = D.id_pdv  LEFT JOIN saxenda AS C ON B.`id_pdv` = C.id_pdv WHERE DATE(B.fecha_registro) = '2020-08-03'");
  echo mysqli_error($conex);

?>
<table border="1px" bordercolor="#000000">
      <tr style="font-weight:bold; text-transform:uppercase; height:25; padding:3px" >
          
		  <th class="botones" style="background-color:#9d1117; color: #ffffff;" >#ID</th>
		  <th class="botones" style="background-color:#9d1117; color: #ffffff;" >COD PDV</th>
          <th class="botones" style="background-color:#9d1117; color: #ffffff;" >NOMBRE PDV</th>
          <th class="botones" style="background-color:#9d1117; color: #ffffff;" >CIUDAD</th>
		  <th class="botones" style="background-color:#9d1117; color: #ffffff;" >DIRECCION</th>
		  <th class="botones" style="background-color:#9d1117; color: #ffffff;" >TELEFONO</th>
		  <th class="botones" style="background-color:#9d1117; color: #ffffff;" >LUGAR DE ENTREGA</th>
          <th class="botones" style="background-color:#9d1117; color: #ffffff;" >CADENA PDV</th>
          <th class="botones" style="background-color:#9d1117; color: #ffffff;" >CANAL PDV</th>
          <th class="botones" style="background-color:#9d1117; color: #ffffff;" >NOMBRE REGENTE</th>
          <th class="botones" style="background-color:#9d1117; color: #ffffff;" >N� REGENTE</th>
		  <th class="botones" style="background-color:#9d1117; color: #ffffff;" >CONVENIO</th>
		  <th class="botones" style="background-color:#9d1117; color: #ffffff;" >CATEGORIA</th>
          <th class="botones" style="background-color:#9d1117; color: #ffffff;" >VISITA</th>
		  <th class="botones" style="background-color:#9d1117; color: #ffffff;" >TIPO_GESTION</th>
		  <th class="botones" style="background-color:#9d1117; color: #ffffff;" >�Alguno de sus familiares con quien comparte su domicilio presenta sospecha o es caso positivo al COVID - 19?</th>
		  <th class="botones" style="background-color:#9d1117; color: #ffffff;" >�Tiene conocimiento del proceso de reporte de casos sospechosos o positivos por COVID-19?</th>
		  <th class="botones" style="background-color:#9d1117; color: #ffffff;" >NOMBRE SUPERVISOR</th>
		  <th class="botones" style="background-color:#9d1117; color: #ffffff;" >DOCUMENTO SUPERVISOR</th>
		  <th class="botones" style="background-color:#9d1117; color: #ffffff;" >CARGO SUPERVISOR</th>
		  <th class="botones" style="background-color:#9d1117; color: #ffffff;" >CIUDAD SUPERVISOR</th>
		  <th class="botones" style="background-color:#9d1117; color: #ffffff;" >FECHA REGISTRO</th>
		  
         
          
      </tr>
	  <?php
      while ($fila1 = mysqli_fetch_array($consulta_cov19))
      { 
      ?>
          <tr align="center" style="height:25px;">
          	  
              
			  <td><?php echo $fila1['id_cov19']; ?></td>
			  <td><?php echo utf8_decode($fila1['names']); ?></td>
			  <td><?php echo utf8_decode($fila1['surnames']); ?></td>
			  <td><?php echo $fila1['documento']?></td>
			  <td><?php echo $fila1['cargo']?></td>
			  <td><?php echo utf8_decode($fila1['ciudad']);?></td>
              <td><?php echo utf8_decode($fila1['ubicacion']);?></td>
			  <td><?php echo utf8_decode($fila1['capacitacion_I']);?></td>
			  <td><?php echo utf8_decode($fila1['capacitacion_II']);?></td>
			  <td><?php echo utf8_decode($fila1['capacitacion_III']);?></td>
			  <td><?php echo $fila1['presenta_sintomas']?></td>
			  <td><?php echo $fila1['desinfecta_equipos']?></td>
			  <td><?php echo $fila1['hidratacion']?></td>
			  <td><?php echo $fila1['temperatura']?></td>
			  <td><?php echo $fila1['elementos_preventivos']?></td>
			  <td><?php echo $fila1['familiares_cov19']?></td>
			  <td><?php echo $fila1['conocimento_cov19']?></td>
              <td><?php echo utf8_decode($fila1['nombre_super'])." ".utf8_decode($fila1['apellido_super']);?></td>
			  <td><?php echo $fila1['documento_super']?></td>
              <td><?php echo utf8_decode($fila1['cargo_super']);?></td>
              <td><?php echo utf8_decode($fila1['ciudad']);?></td>
			  <td><?php echo $fila1['fecha_registro1']?></td>           
  </tr>
              <?php
			 
      }
      ?>
</table>
 