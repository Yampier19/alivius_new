<!--
=========================================================
Material Dashboard - v2.1.2
=========================================================

Product Page: https://www.creative-tim.com/product/material-dashboard
Copyright 2020 Creative Tim (https://www.creative-tim.com)
Coded by Creative Tim

=========================================================
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->
<?php
require('../../../CONNECTION/SECURITY/conex.php');
require('../../../CONNECTION/SECURITY/session_cookie.php');

?>

<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8" />

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>Encuesta</title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />

  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="../../../DESIGN/assets/demo/material-dashboard.min.css" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <script src="../../../DESIGN/JS/jquery-3.5.1.min.js"></script>
  <link rel="stylesheet" href="../../../DESIGN/CSS/main.css">
  <script>
    function causalval() {
      var causal1 = $('#causal1').val();
      var estadoes = $('#estadoes').val();
      if (causal1 == 'Nuevo') {
        $("#fechProxGestion1").css('display', 'block');
        $('#fechProxGestion1').attr('required', 'required');
        $("#fechProxGestion2").css('display', 'none');
        $('#fechProxGestion2').removeAttr('required', 'required');
        $('#fechProxGestion1').attr('readonly', 'readonly');
        $('#fechProxGestion1').removeAttr('disabled', 'disabled');
        $('#fechProxGestion2').attr('disabled', 'disabled');

      } else if (estadoes != 'Retiro') {
        $("#fechProxGestion1").css('display', 'none');
        $('#fechProxGestion1').removeAttr('required', 'required');
        $("#fechProxGestion2").css('display', 'block');
        $('#fechProxGestion2').attr('required', 'required');
        $('#fechProxGestion1').attr('disabled', 'disabled');
        $('#fechProxGestion1').removeAttr('readonly', 'readonly');
        $('#fechProxGestion2').removeAttr('disabled', 'disabled');
      }

    }

    function traerdata() {

      var departamento1 = $('#departamento1').val();
      var estado = '1';

      //solo usa para activar el ajax
      $.ajax({
        url: '../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ajax_ciudad.php',
        data: {
          departamento1: departamento1,
          estado: estado

        },
        type: 'post',
        beforesend: function() {},
        success: function(data) {
          $('#ciudad').html(data);

        }
      });

    }



    function traerdata2() {
      var estado = '2';


      //solo usa para activar el ajax
      $.ajax({
        url: '../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ajax_ciudad.php',
        data: {
          estado: estado

        },
        type: 'post',
        beforesend: function() {},
        success: function(data) {
          $('#departamento').html(data);
        }
      });
    }

    function traerEstado() {
      var estadoe = '1';

      $.ajax({
        url: '../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ajax_causal.php',
        data: {
          estadoe: estadoe
        },
        type: 'post',
        beforeSend: function() {},
        success: function(data) {
          $('#estado').html(data);
        }
      });
    }

    function traercausal() {
      var estadoes = $('#estadoes').val();
      if (estadoes == 'Retiro') {
        $("#fechProxGestion1").css('display', 'none');
        $("#fechProxGestion2").css('display', 'none');


      }
      //$('#fechlit').attr("value","hola1");
      var estadoe = '2';
      var estadoes = $('#estadoes').val();

      $.ajax({
        url: '../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ajax_causal.php',
        data: {
          estadoe: estadoe,
          estadoes: estadoes
        },
        type: 'post',
        beforeSend: function() {},
        success: function(data) {
          $('#causal').html(data);
        }
      });
    }
  </script>
  <script>
    traerdata2();
    traerEstado();
  </script>
</head>

<body class="">

  <div class="wrapper">

    <?php

    $idrol = $_SESSION['id_loginrol'];

    if ($idrol == 1) {
     require("../../DROPDOWN/admin_menu.php");
    } else {
      require("../../DROPDOWN/adviser_menu.php");
    }
    ?>

    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper" style="border-radius: 6px; background-color:#FFFFFF">
            <a style="color:#333333;" class="navbar-brand" href="javascript:;">
              <script>
                var f = new Date();
                document.write(f.getDate() + "/" + (f.getMonth() + 1) + "/" + f.getFullYear());
              </script>
            </a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">

            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="javascript:;">
                  <i class="material-icons">dashboard</i>
                  <p class="d-lg-none d-md-block">
                    Stats
                  </p>
                </a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">notifications</i>
                  <span class="notification">5</span>
                  <p class="d-lg-none d-md-block">
                    Some Actions
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="#">Pendiente Punto BAR004</a>
                  <a class="dropdown-item" href="#">Pendiente Punto BAR005</a>
                  <a class="dropdown-item" href="#">Pendiente Punto BAR006</a>
                  <a class="dropdown-item" href="#">Pendiente Punto BAR007</a>
                  <a class="dropdown-item" href="#">Pendiente Punto BAR008</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="javascript:;" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="#">Perfil</a>
                  <a class="dropdown-item" href="#">Configuraci&oacute;n</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="../../../CONNECTION/SECURITY/destroy.php">Cerrar Sesi&oacute;n</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>



      <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header card-header-primary card-header-icon">
                <div class="card-icon">
                  <i class="material-icons">assignment</i>
                </div>
                <h4 class="card-title">PACIENTE NUEVO</h4><br>
              </div>
              <div class="card-body">

                <!--------------------------------------- FORM --------------------------------------->
                <form method="post" enctype="multipart/form-data">
                  <input type="hidden" name="id_user" value="<?php echo $id_user; ?>">
                  <div class="form-row">
                    <!--Inicio del Form -->

                    <!-- INFORMACION DEL PUNTO  -->
                    <div class="col-md-12">
                      <h5 class="mb-12">
                        <a class="collapsed" data-toggle="collapse" href="#collapseFor" aria-expanded="false" aria-controls="collapseFor">

                        </a>
                      </h5>
                    </div>

                    <div class="col-md-3">
                      <label for="inputEmail4">Nombre<span class="text-danger">*</span></label>
                      <input type="text" class="form-control" placeholder="" name="nombre" id="nombre" maxlength="50" required><br>
                    </div>

                    <div class="col-md-3">
                      <label for="inputPassword4">Apellido<span class="text-danger">*</span></label>
                      <input type="text" class="form-control" placeholder="" name="apellido" id="apellido" maxlength="50" required>
                    </div>

                    <div class="col-md-3">
                      <label for="inputAddress">Cedula<span class="text-danger">*</span></label>
                      <input type="number" class="form-control" placeholder="" name="cedula" id="cedula" autocomplete="off" maxlength="20" required>
                    </div>
                    <div class="col-md-3">
                      <label for="inputAddress2">Telefono<span class="text-danger">*</span></label>
                      <input type="text" class="form-control" placeholder="" name="telefono" id="telefono" autocomplete="off" maxlength="20" required>
                    </div>

                    <div class="col-md-6">
                      <label for="inputCity">Direccion<span class="text-danger">*</span></label>
                      <input type="text" class="form-control" placeholder="" name="direccion" id="direccion" autocomplete="off" maxlength="100" required>
                    </div>

                    <div class="col-md-3">
                      <label for="disponibilidad">Departamento<span class="text-danger">*</span></label>
                      <span name="departamento" id="departamento">
                      </span>
                    </div>

                    <div class="col-md-3">
                      <label for="">Ciudad <span class="text-danger">*</span></label>
                      <samp name="ciudad" id="ciudad" maxlength="20">
                        <select class="form-control selectpicker" data-style="btn btn-link" required>
                          <option value="">Seleccione...</option>
                        </select>
                      </samp>
                    </div>


                    <div class="col-md-12">
                      <h5 class="mb-12">
                        <a class="collapsed" data-toggle="collapse" href="#collapseFor" aria-expanded="false" aria-controls="collapseFor">
                        </a>
                      </h5>
                    </div>

                    <div class="col-md-4">
                      <label for="inputCity">IPS<span class="text-danger">*</span></label>
                      <input type="text" class="form-control" placeholder="" name="ips" id="ips" maxlength="50" required>
                    </div>
                    <div class="col-md-3">
                      <label for="inputCity">Cie10<span class="text-danger">*</span></label>
                      <input type="text" class="form-control" placeholder="" name="cie10" id="cie10" maxlength="4" required>
                    </div>

                    <div class="col-md-5">
                      <label for="exampleFormControlTextarea1">Diagnostico<span class="text-danger">*</span></label>
                      <input type="text" class="form-control" name="diagnostico" id="diagnostico" maxlength="80" required>
                    </div>

                    <div class="col-md-12">
                      <h5 class="mb-12">
                        <a class="collapsed" data-toggle="collapse" href="#collapseFor" aria-expanded="false" aria-controls="collapseFor">
                        </a>
                      </h5>
                    </div>

                    <div class="col-md-4">
                      <label for="inputCity">Nombre Cuidador<span class="text-danger">*</span></label>
                      <input type="text" class="form-control" placeholder="" name="nombreCuid" id="nombreCuid" autocomplete="off" maxlength="80" required>
                    </div>
                    <div class="col-md-4">
                      <label for="inputCity">Parentesco<span class="text-danger">*</span></label>
                      <input type="text" class="form-control" placeholder="" name="parentesco" id="parentesco" autocomplete="off" maxlength="20" required>
                    </div>
                    <div class="col-md-4">
                      <label for="inputCity">Telefono Acudiente<span class="text-danger">*</span></label>
                      <input type="text" class="form-control" placeholder="" name="telAcudiente" id="telAcudiente" autocomplete="off" maxlength="20" required>
                    </div>
                    <div class="col-md-12">
                      <h5 class="mb-12">
                        <a class="collapsed" data-toggle="collapse" href="#collapseFor" aria-expanded="false" aria-controls="collapseFor">

                        </a>
                      </h5>
                    </div>
                    <!-- FIN DE INFORMACION DEL PUNTO  -->
                    <!-- INICIO DE PREGUNTAS  -->
                    <div class="col-md-12">
                      <h5 class="mb-12">
                        <a class="collapsed" data-toggle="collapse" href="#collapseFor" aria-expanded="false" aria-controls="collapseFor">
                        </a>
                      </h5>
                    </div>

                    <div class="col-md-4">
                      <label for="disponibilidad">Estado<span class="text-danger">*</span></label>
                      <span name="estado" id="estado">
                      </span>
                    </div>
                    <div class="col-md-4">
                      <label for="disponibilidad">Causal<span class="text-danger">*</span></label>
                      <span name="causal" id="causal">
                        <select class="form-control selectpicker" data-style="btn btn-link" required>
                          <option value="">Seleccione...</option>
                        </select>
                      </span>
                    </div>
                    <div class="col-md-12">
                      <p> </p>
                    </div>
                    <div class="col-md-6">
                      <div class="card ">
                        <div class="card-header card-header-rose card-header-text">
                          <div class="card-icon">
                            <i class="material-icons">today</i>
                          </div>
                          <h4 class="card-title">Fecha de gesti&oacute;n</h4>
                        </div>
                        <div class="card-body ">
                          <div class="form-group bmd-form-group is-filled">
                            <input type="date" class="form-control datetimepicker" name="fechgestion" id="fechgestion" value="<?php date_default_timezone_set("America/Bogota");
                                                                                                                              echo date('Y-m-d'); ?>" readonly>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="card ">
                        <div class="card-header card-header-rose card-header-text">
                          <div class="card-icon">
                            <i class="material-icons">today</i>
                          </div>
                          <h4 class="card-title">Fecha de pr&oacute;xima gesti&oacute;n</h4>
                        </div>
                        <div class="card-body ">
                          <div class="form-group bmd-form-group is-filled">
                            <input type="date" class="form-control datetimepicker" name="fechProxGestion" id="fechProxGestion2" min="<?php echo date('Y-m-d'); ?>" value="<?php echo date('Y-m-d'); ?>">
                            <input type="date" class="form-control datetimepicker" name="fechProxGestion" id="fechProxGestion1" value="<?php $fecha = date('Y-m-d');
                                                                                                                                        $nuevafecha = strtotime('+14 day', strtotime($fecha));
                                                                                                                                        $nuevafecha = date('Y-m-d', $nuevafecha);
                                                                                                                                        echo $nuevafecha; ?>" style="display:none;">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="exampleFormControlTextarea1">Descripcion</label>
                        <textarea class="form-control" rows="6" name="descripcion" id="descripcion"></textarea>
                      </div>
                    </div>



                    <!-- FIN PREGUNTAS -->

                  </div><br> <!-- Fin Form -->

                  <center>
                    <div class="col-md-4"><button id="enviarEncuesta" formaction="../../../FUNCTIONS/CRUD/insert_paci_new.php" type="submit" class="btn btn-success mat-raised-button mat-button-base"><i class="material-icons">verified_user</i> Enviar Informaci&oacute;n </button> </div>
                  </center>
                  <div class="col-md-4"></div>


                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <footer class="footer">

        <div class="container-fluid">

          <div class="copyright float-right">
            &copy;
            <script>
              document.write(new Date().getFullYear());
            </script>
            <a href="https://peoplemarketing.com/inicio/" target="_blank">People Marketing</a>
          </div>
        </div>

      </footer>

    </div>
  </div>

  <!-- end content-->
  <!--   Core JS Files   -->
  <script src="../../../DESIGN/assets/js/core/popper.min.js"></script>

  <script src="../../../DESIGN/assets/js/core/bootstrap-material-design.min.js"></script>
  <script src="../../../DESIGN/assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!-- Plugin for the momentJs -->
  <script src="../../../DESIGN/assets/js/plugins/moment.min.js"></script>
  <!--  Plugin for Sweet Alert -->
  <script src="../../../DESIGN/assets/js/plugins/sweetalert2.js"></script>
  <!-- Forms Validations Plugin -->
  <script src="../../../DESIGN/assets/js/plugins/jquery.validate.min.js"></script>
  <!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
  <script src="../../../DESIGN/assets/js/plugins/jquery.bootstrap-wizard.js"></script>
  <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
  <script src="../../../DESIGN/assets/js/plugins/bootstrap-selectpicker.js"></script>
  <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
  <script src="../../../DESIGN/assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
  <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
  <script src="../../../DESIGN/assets/js/plugins/jquery.dataTables.min.js"></script>
  <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
  <script src="../../../DESIGN/assets/js/plugins/bootstrap-tagsinput.js"></script>
  <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
  <script src="../../../DESIGN/assets/js/plugins/jasny-bootstrap.min.js"></script>
  <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
  <script src="../../../DESIGN/assets/js/plugins/fullcalendar.min.js"></script>
  <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
  <script src="../../../DESIGN/assets/js/plugins/jquery-jvectormap.js"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="../../../DESIGN/assets/js/plugins/nouislider.min.js"></script>
  <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
  <!-- Library for adding dinamically elements -->
  <script src="../../../DESIGN/assets/js/plugins/arrive.min.js"></script>
  <!--  Google Maps Plugin    
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  Chartist JS -->
  <script src="../../../DESIGN/assets/js/plugins/chartist.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="../../../DESIGN/assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="../../../DESIGN/assets/js/material-dashboard.js?v=2.1.2" type="text/javascript"></script>
  <!-- Material Dashboard DEMO methods, don't include it in your project! -->
  <!-- Script -->

</body>

</html>