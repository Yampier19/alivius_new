<!--
=========================================================
Material Dashboard - v2.1.2
=========================================================

Product Page: https://www.creative-tim.com/product/material-dashboard
Copyright 2020 Creative Tim (https://www.creative-tim.com)
Coded by Creative Tim

=========================================================
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->
<?php
require('../../../CONNECTION/SECURITY/conex.php');
require('../../../CONNECTION/SECURITY/session_cookie.php');

?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Administrar Usuarios</title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />

    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="../../../DESIGN/assets/demo/material-dashboard.min.css" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <script src="../../../DESIGN/JS/jquery-3.5.1.min.js"></script>
    <link rel="stylesheet" href="../../../DESIGN/CSS/main.css">
    
    <script>
        function causalval() {
            var causal1 = $('#causal1').val();
            var estadoes = $('#estadoes').val();
            if (causal1 == 'Nuevo') {
                $("#fechProxGestion1").css('display', 'block');
                $('#fechProxGestion1').attr('required', 'required');
                $("#fechProxGestion2").css('display', 'none');
                $('#fechProxGestion2').removeAttr('required', 'required');
                $('#fechProxGestion1').attr('readonly', 'readonly');
                $('#fechProxGestion1').removeAttr('disabled', 'disabled');
                $('#fechProxGestion2').attr('disabled', 'disabled');

            } else if (estadoes != 'Retiro') {
                $("#fechProxGestion1").css('display', 'none');
                $('#fechProxGestion1').removeAttr('required', 'required');
                $("#fechProxGestion2").css('display', 'block');
                $('#fechProxGestion2').attr('required', 'required');
                $('#fechProxGestion1').attr('disabled', 'disabled');
                $('#fechProxGestion1').removeAttr('readonly', 'readonly');
                $('#fechProxGestion2').removeAttr('disabled', 'disabled');
            }

        }

        function traerdata() {

            var departamento1 = $('#departamento1').val();
            var estado = '1';

            //solo usa para activar el ajax
            $.ajax({
                url: '../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ajax_ciudad.php',
                data: {
                    departamento1: departamento1,
                    estado: estado

                },
                type: 'post',
                beforesend: function() {},
                success: function(data) {
                    $('#ciudad').html(data);

                }
            });

        }



        function traerdata2() {
            var estado = '2';


            //solo usa para activar el ajax
            $.ajax({
                url: '../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ajax_ciudad.php',
                data: {
                    estado: estado

                },
                type: 'post',
                beforesend: function() {},
                success: function(data) {
                    $('#departamento').html(data);
                }
            });
        }

        function traerEstado() {
            var estadoe = '1';

            $.ajax({
                url: '../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ajax_causal.php',
                data: {
                    estadoe: estadoe
                },
                type: 'post',
                beforeSend: function() {},
                success: function(data) {
                    $('#estado').html(data);
                }
            });
        }

        function traercausal() {
            var estadoes = $('#estadoes').val();
            if (estadoes == 'Retiro') {
                $("#fechProxGestion1").css('display', 'none');
                $("#fechProxGestion2").css('display', 'none');


            }
            //$('#fechlit').attr("value","hola1");
            var estadoe = '2';
            var estadoes = $('#estadoes').val();

            $.ajax({
                url: '../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ajax_causal.php',
                data: {
                    estadoe: estadoe,
                    estadoes: estadoes
                },
                type: 'post',
                beforeSend: function() {},
                success: function(data) {
                    $('#causal').html(data);
                }
            });
        }
    </script>
    <script>
        traerdata2();
        traerEstado();
    </script>
</head>

<body class="">

    <div class="wrapper">

        <?php

        $idrol = $_SESSION['id_loginrol'];

        if ($idrol == 1) {
            require("../../DROPDOWN/admin_menu.php");
        } else {
            require("../../DROPDOWN/adviser_menu.php");
        }
        ?>

        <div class="main-panel">
            <!-- Navbar -->
            <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
                <div class="container-fluid">
                    <div class="navbar-wrapper" style="border-radius: 6px; background-color:#FFFFFF">
                        <a style="color:#333333;" class="navbar-brand" href="javascript:;">
                            <script>
                                var f = new Date();
                                document.write(f.getDate() + "/" + (f.getMonth() + 1) + "/" + f.getFullYear());
                            </script>
                        </a>
                    </div>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="navbar-toggler-icon icon-bar"></span>
                        <span class="navbar-toggler-icon icon-bar"></span>
                        <span class="navbar-toggler-icon icon-bar"></span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end">

                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link" href="javascript:;">
                                    <i class="material-icons">dashboard</i>
                                    <p class="d-lg-none d-md-block">
                                        Stats
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">notifications</i>
                                    <span class="notification">5</span>
                                    <p class="d-lg-none d-md-block">
                                        Some Actions
                                    </p>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item" href="#">Pendiente Punto BAR004</a>
                                    <a class="dropdown-item" href="#">Pendiente Punto BAR005</a>
                                    <a class="dropdown-item" href="#">Pendiente Punto BAR006</a>
                                    <a class="dropdown-item" href="#">Pendiente Punto BAR007</a>
                                    <a class="dropdown-item" href="#">Pendiente Punto BAR008</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="javascript:;" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">person</i>
                                    <p class="d-lg-none d-md-block">
                                        Account
                                    </p>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                                    <a class="dropdown-item" href="#">Perfil</a>
                                    <a class="dropdown-item" href="#">Configuraci&oacute;n</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="../../../CONNECTION/SECURITY/destroy.php">Cerrar Sesi&oacute;n</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>



            <div class="content">
                <div class="row">
                    <div class="col-md-12">
                      <iframe style="border:none" width="100%" height="800px" src="../REPORTS/menu_app"></iframe> 
                    </div>
                </div>
            </div>


            <?php
            $id = null;
            $resultado = mysqli_query($conex, "SELECT * FROM user u INNER JOIN userlogin ul ON ul.id_user = u.id_user");
            while ($usuarios = mysqli_fetch_array($resultado)) {
                $id_user = $usuarios['id_user'];
                $nombre = $usuarios['names'];
                $apellido = $usuarios['surnames'];
                $documento = $usuarios['documento'];
                $correo = $usuarios['correo'];
                $name_user = $usuarios['name_user'];
                $pass = $usuarios['password'];

            ?>

                <!------------------------------------ Modal Eliminar -------------------------------------->
                <div class="modal fade" id="delete<?= $usuarios['id_user'] ?>" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-signup" role="document">
                        <div class="modal-content">
                            <div class="card card-signup card-plain">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="updateLabel"><b>Eliminar usuario</b></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <i class="material-icons">close</i>
                                    </button>
                                </div>
                                <div class="modal-body mt-5">
                                <form action="./../../../FUNCTIONS/CRUD/delete_adviser.php?id=<?=$usuarios['id_log']?>" method="post">
                                    <h3 class="modal-title" id="updateLabel"><b>¿Quieres eliminar el usuario: <?= $nombre . " " . $apellido; ?>?</b></h3>
                                    <div class="row col-md-12 mt-5">
                                        <button  style="background-color:#d4891cdb;" class="btn btn mat-raised-button mat-button-base col-md-5 mx-auto" data-dismiss="modal" aria-label="Close"><i class="material-icons" >cancel</i> Cancelar </button>
                                        <input id="submit" name="submit" type="submit" value="Eliminar Usuario" style="background-color: #ce3232;" class="btn btn mat-raised-button mat-button-base col-md-5 mx-auto" />
                                       
                                    </div>
                                    </form>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
                <!------------------------------------ Final Modal Delete ---------------------------------->

                <!------------------------------------ Modal Actualizar ------------------------------------>
                <div class="modal fade" id="update<?= $usuarios['id_user'] ?>" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-signup" role="document">
                        <div class="modal-content">
                            <div class="card card-signup card-plain">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel"><b>Actualizar Usuario: <?= $nombre . " " . $apellido; ?></b></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <i class="material-icons">close</i>
                                    </button>
                                </div>
                                <div class="modal-body mt-5">
                                    <form action="./../../../FUNCTIONS/CRUD/updateAdviser.php?id=<?= $usuarios['id_user'] ?>" method="post">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="inputEmail4">Nombre<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" placeholder="" name="names" id="names" maxlength="50" value="<?= $nombre; ?>"><br>
                                            </div>

                                            <div class="col-md-6">
                                                <label for="inputPassword4">Apellido<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" placeholder="" name="surnames" id="surnames" maxlength="50" value="<?= $apellido; ?>">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="inputEmail4">Documento<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" placeholder="" name="documento" id="names" maxlength="50" value="<?= $documento; ?>"><br>
                                            </div>

                                            <div class="col-md-6">
                                                <label for="inputPassword4">Correo<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" placeholder="" name="correo" id="surnames" maxlength="50" value="<?= $correo; ?>">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="inputEmail4">Nombre de usuario<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" placeholder="" name="name_user" id="names" maxlength="50" value="<?= $name_user; ?>"><br>
                                            </div>

                                            <div class="col-md-6">
                                                <label for="inputPassword4">Contraseña<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" placeholder="" name="password" id="surnames" maxlength="50">
                                            </div>
                                        </div>
                                        <div class="row col-md-12">
                                            <button id="submit" name="submit" type="submit" style="background-color: #2a8454;" class="btn btn mat-raised-button mat-button-base col-md-6 mx-auto"><i class="material-icons">verified_user</i> Actualizar Usuario </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <!------------------------------------ Fin Modal Actualizar ---------------------------------->


            <footer class="footer">

                <div class="container-fluid">

                    <div class="copyright float-right">
                        &copy;
                        <script>
                            document.write(new Date().getFullYear());
                        </script>
                        <a href="https://peoplemarketing.com/inicio/" target="_blank">People Marketing</a>
                    </div>
                </div>

            </footer>

        </div>
    </div>


    <script>
        function mostrar_div(id, clase) {
            var main = ("#" + id);
            if (clase > 0) main = ("." + id)
            $(main).toggle('normal');
        }
    </script>

<script>
    $(document).ready(function() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "Tot"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Buscar registros",
        }
      });

      var table = $('#datatable').DataTable();

      // Edit record
      table.on('click', '.edit', function() {
        $tr = $(this).closest('tr');
        var data = table.row($tr).data();
        alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      });

      // Delete a record
      table.on('click', '.remove', function(e) {
        $tr = $(this).closest('tr');
        table.row($tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function() {
        alert('You clicked on Like button');
      });
    });
  </script>


    <!-- end content-->
    <!--   Core JS Files   -->
    <script src="../../../DESIGN/assets/js/core/popper.min.js"></script>

    <script src="../../../DESIGN/assets/js/core/bootstrap-material-design.min.js"></script>
    <script src="../../../DESIGN/assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
    <!-- Plugin for the momentJs -->
    <script src="../../../DESIGN/assets/js/plugins/moment.min.js"></script>
    <!--  Plugin for Sweet Alert -->
    <script src="../../../DESIGN/assets/js/plugins/sweetalert2.js"></script>
    <!-- Forms Validations Plugin -->
    <script src="../../../DESIGN/assets/js/plugins/jquery.validate.min.js"></script>
    <!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
    <script src="../../../DESIGN/assets/js/plugins/jquery.bootstrap-wizard.js"></script>
    <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
    <script src="../../../DESIGN/assets/js/plugins/bootstrap-selectpicker.js"></script>
    <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
    <script src="../../../DESIGN/assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
    <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
    <script src="../../../DESIGN/assets/js/plugins/jquery.dataTables.min.js"></script>
    <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
    <script src="../../../DESIGN/assets/js/plugins/bootstrap-tagsinput.js"></script>
    <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
    <script src="../../../DESIGN/assets/js/plugins/jasny-bootstrap.min.js"></script>
    <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
    <script src="../../../DESIGN/assets/js/plugins/fullcalendar.min.js"></script>
    <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
    <script src="../../../DESIGN/assets/js/plugins/jquery-jvectormap.js"></script>
    <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
    <script src="../../../DESIGN/assets/js/plugins/nouislider.min.js"></script>
    <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
    <!-- Library for adding dinamically elements -->
    <script src="../../../DESIGN/assets/js/plugins/arrive.min.js"></script>
    <!--  Google Maps Plugin    
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  Chartist JS -->
    <script src="../../../DESIGN/assets/js/plugins/chartist.min.js"></script>
    <!--  Notifications Plugin    -->
    <script src="../../../DESIGN/assets/js/plugins/bootstrap-notify.js"></script>
    <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="../../../DESIGN/assets/js/material-dashboard.js?v=2.1.2" type="text/javascript"></script>
    <!-- Material Dashboard DEMO methods, don't include it in your project! -->
    <!-- Script -->

</body>

</html>